package com.butul0ve.eaptekatest.di.module

import android.content.Context
import com.butul0ve.eaptekatest.di.scope.PicassoScope
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response

@Module
class PicassoModule {

    @PicassoScope
    @Provides
    fun providePicasso(context: Context, okHttp3Downloader: OkHttp3Downloader): Picasso {
        return Picasso.Builder(context)
            .downloader(okHttp3Downloader)
            .build()
    }

    @PicassoScope
    @Provides
    fun provideOkHttp3Downloader(): OkHttp3Downloader {
        return OkHttp3Downloader(
            OkHttpClient.Builder()
                .addInterceptor(object : Interceptor {

                    override fun intercept(chain: Interceptor.Chain): Response {
                        val request = chain.request().newBuilder()
                            .addHeader("Authorization", "Basic ZWFwdGVrYTpzdGFnZQ==")
                            .addHeader("api-key", "i&j*3&^2TZ&d")
                            .addHeader("platform", "android")
                            .addHeader("Content-Type", "application/json")
                            .build()
                        return chain.proceed(request)
                    }
                })
                .build()
        )
    }
}