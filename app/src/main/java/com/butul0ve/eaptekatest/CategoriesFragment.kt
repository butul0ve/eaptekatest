package com.butul0ve.eaptekatest

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.butul0ve.eaptekatest.adapter.CategoryAdapter
import com.butul0ve.eaptekatest.adapter.CategoryClickListener
import com.butul0ve.eaptekatest.model.Category
import com.butul0ve.eaptekatest.model.Product
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_categories.*
import timber.log.Timber
import java.lang.ClassCastException
import java.lang.IllegalStateException
import java.util.ArrayList
import javax.inject.Inject

class CategoriesFragment : Fragment(), CategoryClickListener {

    @Inject
    lateinit var cacheController: CacheController

    private val disposable = CompositeDisposable()
    private lateinit var adapter: CategoryAdapter
    private lateinit var categories: List<Category>
    private lateinit var interactor: OnCategoriesFragmentInteractor
    private lateinit var progressBar: ProgressBar

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            interactor = context as OnCategoriesFragmentInteractor
        } catch (ex: ClassCastException) {
            throw IllegalStateException("host activity must implement OnCategoriesFragmentInteractor")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null && arguments!!.containsKey(CATEGORIES_EXTRA)) {
            categories = arguments!!.getParcelableArrayList(CATEGORIES_EXTRA)!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_categories, null, false)
        progressBar = view.findViewById(R.id.progress_bar)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = activity?.applicationContext as EaptekaApplication
        app.component.inject(this)

        recycler_view.setHasFixedSize(true)

        if (::categories.isInitialized) {
            recycler_view.adapter = CategoryAdapter(categories, this)
            return
        }

        disposable.add(cacheController.getCategories()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                progressBar.visibility = View.VISIBLE
            }
            .subscribe { t1, t2 ->
                progressBar.visibility = View.GONE
                if (t1 != null) {
                    categories = t1
                    adapter = CategoryAdapter(categories, this)
                    recycler_view.adapter = adapter
                }
                if (t2 != null) {
                    Timber.d(t2)
                }
            })
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }

    override fun onCategoryClick(position: Int) {
        if (!::categories.isInitialized) return

        val category = categories[position]
        if (category.subcategories && category.id != null) {
            val subscribe = cacheController.getCategories(category.id!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    progressBar.visibility = View.VISIBLE
                }
                .subscribe { t1, t2 ->
                    progressBar.visibility = View.GONE
                    if (t1 != null) {
                        interactor.openNewFragmentWithCategories(t1)
                    }

                    if (t2 != null) {
                        Timber.d(t2)
                    }
                }
            subscribe?.let { disposable.add(it) }
        } else if (category.id != null) {
            cacheController.getProducts(category.id!!)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    progressBar.visibility = View.VISIBLE
                }
                .subscribe { t1, t2 ->
                    progressBar.visibility = View.GONE
                    if (t1 != null) {
                        interactor.openNewFragmentWithProducts(t1)
                    }

                    if (t2 != null) {
                        Timber.d(t2)
                    }
                }
        }
    }

    interface OnCategoriesFragmentInteractor {

        fun openNewFragmentWithCategories(categories: List<Category>)

        fun openNewFragmentWithProducts(products: List<Product>)
    }

    companion object {

        private const val CATEGORIES_EXTRA = "categories_extra"

        fun newInstance(categories: List<Category>): CategoriesFragment {
            val fragment = CategoriesFragment()
            val arguments = Bundle()
            arguments.putParcelableArrayList(CATEGORIES_EXTRA, categories as ArrayList<Category>)
            fragment.arguments = arguments
            return fragment
        }
    }
}