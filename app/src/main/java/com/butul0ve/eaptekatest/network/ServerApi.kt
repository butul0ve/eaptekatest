package com.butul0ve.eaptekatest.network

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface ServerApi {

    @Headers(
        "Authorization: Basic ZWFwdGVrYTpzdGFnZQ==",
        "api-key: i&j*3&^2TZ&d",
        "platform: android",
        "Content-Type: application/json"
    )
    @GET("categories/0")
    fun getBaseCategories(): Single<CategoriesResponse>

    @Headers(
        "Authorization: Basic ZWFwdGVrYTpzdGFnZQ==",
        "api-key: i&j*3&^2TZ&d",
        "platform: android",
        "Content-Type: application/json"
    )
    @GET("categories/{id}")
    fun getCategories(@Path("id") id: Long): Single<CategoriesResponse>

    @Headers(
        "Authorization: Basic ZWFwdGVrYTpzdGFnZQ==",
        "api-key: i&j*3&^2TZ&d",
        "platform: android",
        "Content-Type: application/json"
    )
    @GET("categories/{id}/offers")
    fun getOffers(@Path("id") id: Long): Single<ProductResponse>
}