package com.butul0ve.eaptekatest.db.converter

import android.arch.persistence.room.TypeConverter

class PicturesConverter {

    @TypeConverter
    fun fromList(list: List<String>): String {
        return list.joinToString(separator = ", ")
    }

    @TypeConverter
    fun toList(string: String): List<String> {
        return string.split(", ")
    }
}