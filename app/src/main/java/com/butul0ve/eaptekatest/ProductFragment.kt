package com.butul0ve.eaptekatest

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.butul0ve.eaptekatest.model.Product
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_product.*
import javax.inject.Inject

class ProductFragment : Fragment() {

    @Inject
    lateinit var picasso: Picasso

    private lateinit var product: Product

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null && arguments!!.containsKey(PRODUCT_EXTRA)) {
            product = arguments!!.getParcelable(PRODUCT_EXTRA)!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_product, null, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = activity?.applicationContext as EaptekaApplication
        app.component.inject(this)
        if (::product.isInitialized) {
            product_id.text = product.id.toString()
            product_name.text = product.name
            product.pictures?.get(0)?.let {
                picasso.load(it).into(product_image)
            }
        }
    }

    companion object {

        private const val PRODUCT_EXTRA = "product_extra"

        fun newInstance(product: Product): ProductFragment {
            val fragment = ProductFragment()
            val arguments = Bundle()
            arguments.putParcelable(PRODUCT_EXTRA, product)
            fragment.arguments = arguments
            return fragment
        }
    }
}