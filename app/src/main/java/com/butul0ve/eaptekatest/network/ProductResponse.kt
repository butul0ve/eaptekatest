package com.butul0ve.eaptekatest.network

import com.butul0ve.eaptekatest.model.Product

data class ProductResponse(val offers: List<Product>?)