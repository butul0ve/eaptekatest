package com.butul0ve.eaptekatest.adapter

interface CategoryClickListener {

    fun onCategoryClick(position: Int)
}