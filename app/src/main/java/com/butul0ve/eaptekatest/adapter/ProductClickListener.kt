package com.butul0ve.eaptekatest.adapter

interface ProductClickListener {

    fun onProductClick(position: Int)
}