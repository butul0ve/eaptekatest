package com.butul0ve.eaptekatest

import android.app.Application
import com.butul0ve.eaptekatest.di.DaggerNetComponent
import com.butul0ve.eaptekatest.di.NetComponent
import com.butul0ve.eaptekatest.di.module.AppModule
import com.butul0ve.eaptekatest.di.module.DataModule
import com.butul0ve.eaptekatest.di.module.NetModule
import com.butul0ve.eaptekatest.di.module.PicassoModule
import timber.log.Timber
import timber.log.Timber.plant

class EaptekaApplication : Application() {

    val component: NetComponent by lazy {
        DaggerNetComponent.builder()
            .appModule(AppModule(this))
            .dataModule(DataModule())
            .netModule(NetModule("https://stage.eapteka.ru/api/v2/"))
            .picassoModule(PicassoModule())
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            plant(Timber.DebugTree())
        }
    }
}