package com.butul0ve.eaptekatest.di

import com.butul0ve.eaptekatest.CategoriesFragment
import com.butul0ve.eaptekatest.ProductFragment
import com.butul0ve.eaptekatest.ProductsFragment
import com.butul0ve.eaptekatest.di.module.AppModule
import com.butul0ve.eaptekatest.di.module.DataModule
import com.butul0ve.eaptekatest.di.module.NetModule
import com.butul0ve.eaptekatest.di.module.PicassoModule
import com.butul0ve.eaptekatest.di.scope.PicassoScope
import dagger.Component

@Component(
    modules = [AppModule::class,
        DataModule::class,
        NetModule::class,
        PicassoModule::class]
)
@PicassoScope
interface NetComponent {

    fun inject(fragment: ProductsFragment)

    fun inject(fragment: CategoriesFragment)

    fun inject(fragment: ProductFragment)
}