package com.butul0ve.eaptekatest.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters
import com.butul0ve.eaptekatest.db.converter.SubCategoryConverter

@Entity(tableName = "subcategories")
data class SubCategory(@PrimaryKey var id: Long?,

                       @TypeConverters(SubCategoryConverter::class)
                       var categories: List<Category>)