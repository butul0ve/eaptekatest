package com.butul0ve.eaptekatest.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.butul0ve.eaptekatest.model.Category
import io.reactivex.Single

@Dao
interface CategoriesDao {

    @Query("select * from categories")
    fun getAll(): Single<List<Category>>

    @Query("select * from categories where id = :id limit 1")
    fun getById(id: Long): Single<Category>

    @Insert(onConflict = REPLACE)
    fun put(categories: List<Category>)
}