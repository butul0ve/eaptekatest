package com.butul0ve.eaptekatest

import com.butul0ve.eaptekatest.db.DbHelper
import com.butul0ve.eaptekatest.model.Category
import com.butul0ve.eaptekatest.model.Product
import com.butul0ve.eaptekatest.model.SubCategory
import com.butul0ve.eaptekatest.network.ServerApi
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class CacheController(
    private val dbHelper: DbHelper,
    private val serverApi: ServerApi
) {

    fun getCategories(id: Long = -1L): Single<List<Category>> {
        return Observable.concat(
            getCategoriesFromDb(id),
            getCategoriesFromNetwork(id)
        ).filter { it.isNotEmpty() }
            .first(emptyList())
    }

    fun getProducts(id: Long = -1L): Single<List<Product>> {
        return Observable.concat(
            getProductsFromDb(id),
            getProductsFromNetwork(id)
        )
            .filter { it.isNotEmpty() }
            .first(emptyList())
    }

    private fun getCategoriesFromDb(id: Long): Observable<List<Category>> {
        return if (id == -1L) dbHelper.getCategories().toObservable()
        else dbHelper.getSubCategories(id).map { it.categories }.toObservable()
    }

    private fun getCategoriesFromNetwork(id: Long): Observable<List<Category>> {
        return if (id == -1L) serverApi.getBaseCategories().map { it.categories }.toObservable().doOnNext {
            dbHelper.putCategories(it)
                .subscribeOn(Schedulers.io())
                .subscribe()
        }
        else serverApi.getCategories(id).map { it.categories }.toObservable().doOnNext {
            dbHelper.putSubCategories(id, SubCategory(id, it))
                .subscribeOn(Schedulers.io())
                .subscribe()
        }
    }

    private fun getProductsFromDb(id: Long): Observable<List<Product>> {
        return if (id == -1L) Observable.just(listOf(Product(null, "no data", null, null, null)))
        else dbHelper.getProducts(id).toObservable()
    }

    private fun getProductsFromNetwork(id: Long): Observable<List<Product>> {
        return if (id == -1L) Observable.just(listOf(Product(null, "no data", null, null, null)))
        else serverApi.getOffers(id).map { it.offers!! }.toObservable().doOnNext {
            it.forEach { product -> product.categoryId = id }
            dbHelper.putProducts(it)
                .subscribeOn(Schedulers.io())
                .subscribe()
        }
    }
}