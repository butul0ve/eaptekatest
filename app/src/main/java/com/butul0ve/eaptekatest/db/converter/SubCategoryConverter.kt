package com.butul0ve.eaptekatest.db.converter

import android.arch.persistence.room.TypeConverter
import com.butul0ve.eaptekatest.model.Category
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class SubCategoryConverter {

    @TypeConverter
    fun convertToList(string: String): List<Category> {
        val type = object : TypeToken<List<Category>>(){}.type
        return Gson().fromJson(string, type)
    }

    @TypeConverter
    fun convertFromList(categories: List<Category>): String {
        return Gson().toJson(categories)
    }

}