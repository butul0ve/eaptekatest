package com.butul0ve.eaptekatest

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.butul0ve.eaptekatest.model.Category
import com.butul0ve.eaptekatest.model.Product

class MainActivity : AppCompatActivity(),
    CategoriesFragment.OnCategoriesFragmentInteractor,
    ProductsFragment.OnProductsFragmentInteractor {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.container, CategoriesFragment())
                commit()
            }
        }
    }

    override fun openNewFragmentWithCategories(categories: List<Category>) {
        supportFragmentManager.beginTransaction().apply {
            addToBackStack(null)
            replace(R.id.container, CategoriesFragment.newInstance(categories))
            commit()
        }
    }

    override fun openNewDetailFragmentWithProduct(product: Product) {
        supportFragmentManager.beginTransaction().apply {
            addToBackStack(null)
            replace(R.id.container, ProductFragment.newInstance(product))
            commit()
        }
    }

    override fun openNewFragmentWithProducts(products: List<Product>) {
        supportFragmentManager.beginTransaction().apply {
            addToBackStack(null)
            replace(R.id.container, ProductsFragment.newInstance(products))
            commit()
        }
    }
}