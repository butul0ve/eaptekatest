package com.butul0ve.eaptekatest.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "categories")
@Parcelize
data class Category(
    @PrimaryKey var id: Long?,
    var sort: Long?,
    var name: String?,
    var adultsOnly: Boolean,
    var subcategories: Boolean
) : Parcelable