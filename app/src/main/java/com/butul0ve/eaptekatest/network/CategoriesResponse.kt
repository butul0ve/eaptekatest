package com.butul0ve.eaptekatest.network

import com.butul0ve.eaptekatest.model.Category

data class CategoriesResponse(var categories: List<Category>)