package com.butul0ve.eaptekatest

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.butul0ve.eaptekatest.adapter.ProductAdapter
import com.butul0ve.eaptekatest.adapter.ProductClickListener
import com.butul0ve.eaptekatest.model.Product
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_products.*
import java.lang.ClassCastException
import java.lang.IllegalStateException
import java.util.ArrayList
import javax.inject.Inject

class ProductsFragment: Fragment(), ProductClickListener {

    @Inject
    lateinit var picasso: Picasso

    private lateinit var interactor: OnProductsFragmentInteractor
    private lateinit var products: List<Product>

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            interactor = context as OnProductsFragmentInteractor
        } catch (ex: ClassCastException) {
            throw IllegalStateException("host activity must implement OnProductsFragmentInteractor")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null && arguments!!.containsKey(PRODUCTS_EXTRA)) {
            products = arguments!!.getParcelableArrayList(PRODUCTS_EXTRA)!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_products, null, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val app = activity?.applicationContext as EaptekaApplication
        app.component.inject(this)

        if (::products.isInitialized) {
            products_recycler_view.adapter = ProductAdapter(this,
                products,
                picasso)
        }
    }

    override fun onProductClick(position: Int) {
        interactor.openNewDetailFragmentWithProduct(products[position])
    }

    interface OnProductsFragmentInteractor {

        fun openNewDetailFragmentWithProduct(product: Product)
    }

    companion object {

        private const val PRODUCTS_EXTRA = "products_extra"

        fun newInstance(products: List<Product>): ProductsFragment {
            val fragment = ProductsFragment()
            val arguments = Bundle()
            arguments.putParcelableArrayList(PRODUCTS_EXTRA, products as ArrayList)
            fragment.arguments = arguments
            return fragment
        }
    }
}