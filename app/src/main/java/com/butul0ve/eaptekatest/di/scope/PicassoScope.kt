package com.butul0ve.eaptekatest.di.scope

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.BINARY)
annotation class PicassoScope