package com.butul0ve.eaptekatest.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.butul0ve.eaptekatest.R
import com.butul0ve.eaptekatest.model.Product
import com.squareup.picasso.Picasso

class ProductViewHolder(
    itemView: View,
    private val clickListener: ProductClickListener,
    private val picasso: Picasso
) : RecyclerView.ViewHolder(itemView), View.OnClickListener {


    private val id: TextView = itemView.findViewById(R.id.product_id)
    private val name: TextView = itemView.findViewById(R.id.product_name)
    private val image: ImageView = itemView.findViewById(R.id.product_image)

    init {
        itemView.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        clickListener.onProductClick(adapterPosition)
    }

    fun bind(product: Product) {
        id.text = product.id.toString()
        name.text = product.name
        product.icon?.let {
            picasso
                .load(product.icon)
                .centerCrop()
                .resize(200, 200)
                .into(image)
        }
    }
}