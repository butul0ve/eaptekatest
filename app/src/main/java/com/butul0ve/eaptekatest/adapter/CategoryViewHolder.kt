package com.butul0ve.eaptekatest.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.butul0ve.eaptekatest.R
import com.butul0ve.eaptekatest.model.Category

class CategoryViewHolder(
    itemView: View,
    private val clickListener: CategoryClickListener
) : RecyclerView.ViewHolder(itemView),
    View.OnClickListener {

    private val id: TextView = itemView.findViewById(R.id.category_id)
    private val name: TextView = itemView.findViewById(R.id.category_name)

    init {
        itemView.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        clickListener.onCategoryClick(adapterPosition)
    }

    fun bind(category: Category) {
        id.text = category.id.toString()
        name.text = category.name
    }
}