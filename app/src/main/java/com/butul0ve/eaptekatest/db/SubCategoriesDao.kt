package com.butul0ve.eaptekatest.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.butul0ve.eaptekatest.model.SubCategory
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface SubCategoriesDao {

    @Query("select * from subcategories")
    fun getAll(): Single<List<SubCategory>>

    @Insert(onConflict = REPLACE)
    fun put(subCategories: SubCategory)

    @Query("select * from subcategories where id = :id limit 1")
    fun getById(id: Long): Maybe<SubCategory>
}