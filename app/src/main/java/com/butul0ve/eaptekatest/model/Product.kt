package com.butul0ve.eaptekatest.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters
import android.os.Parcelable
import com.butul0ve.eaptekatest.db.converter.PicturesConverter
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "products")
@Parcelize
class Product(
    @PrimaryKey var id: Long?,
    var name: String?,
    var icon: String?,

    @TypeConverters(PicturesConverter::class)
    var pictures: List<String>?,

    var categoryId: Long?
) : Parcelable