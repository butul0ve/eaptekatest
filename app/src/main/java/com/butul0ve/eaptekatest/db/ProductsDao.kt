package com.butul0ve.eaptekatest.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.butul0ve.eaptekatest.model.Product
import io.reactivex.Flowable
import io.reactivex.Maybe

@Dao
interface ProductsDao {

    @Query("select * from products where categoryId = :categoryId ")
    fun getAll(categoryId: Long): Maybe<List<Product>>

    @Query("select * from products where id = :id limit 1")
    fun getById(id: Long): Maybe<Product>

    @Insert(onConflict = REPLACE)
    fun put(products: List<Product>)
}