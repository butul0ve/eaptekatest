package com.butul0ve.eaptekatest.di.module

import android.content.Context
import com.butul0ve.eaptekatest.CacheController
import com.butul0ve.eaptekatest.db.DbHelper
import com.butul0ve.eaptekatest.db.EaptekaDb
import com.butul0ve.eaptekatest.network.ServerApi
import dagger.Module
import dagger.Provides

@Module
class DataModule {

    @Provides
    fun provideDB(context: Context) = EaptekaDb.getInstance(context)!!

    @Provides
    fun provideDbHelper(db: EaptekaDb) = DbHelper(db)

    @Provides
    fun provideCacheController(
        dbHelper: DbHelper,
        serverApi: ServerApi
    ): CacheController {
        return CacheController(dbHelper, serverApi)
    }
}