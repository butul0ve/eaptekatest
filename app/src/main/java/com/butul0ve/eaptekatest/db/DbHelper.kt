package com.butul0ve.eaptekatest.db

import com.butul0ve.eaptekatest.model.Category
import com.butul0ve.eaptekatest.model.Product
import com.butul0ve.eaptekatest.model.SubCategory
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class DbHelper(private val db: EaptekaDb) {

    fun getProducts(categoryId: Long): Maybe<List<Product>> {
        return db.productsDao().getAll(categoryId)
            .subscribeOn(Schedulers.io())
    }

    fun getCategories(): Single<List<Category>> {
        return db.categoriesDao().getAll()
            .subscribeOn(Schedulers.io())
    }

    fun putCategories(categories: List<Category>): Completable {
        return Completable.fromAction { db.categoriesDao().put(categories) }
    }

    fun getSubCategories(id: Long): Maybe<SubCategory> {
        return db.subcategoriesDao().getById(id)
            .subscribeOn(Schedulers.io())
    }

    fun putSubCategories(id: Long, subCategory: SubCategory): Completable {
        return Completable.fromAction { db.subcategoriesDao().put(subCategory) }
    }

    fun putProducts(offers: List<Product>): Completable {
        return Completable.fromAction { db.productsDao().put(offers) }
    }
}