package com.butul0ve.eaptekatest.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context
import com.butul0ve.eaptekatest.db.converter.PicturesConverter
import com.butul0ve.eaptekatest.db.converter.SubCategoryConverter
import com.butul0ve.eaptekatest.model.Category
import com.butul0ve.eaptekatest.model.Product
import com.butul0ve.eaptekatest.model.SubCategory

@Database(entities = [Category::class, Product::class, SubCategory::class], version = 1, exportSchema = false)
@TypeConverters(SubCategoryConverter::class, PicturesConverter::class)
abstract class EaptekaDb : RoomDatabase() {

    abstract fun categoriesDao(): CategoriesDao
    abstract fun productsDao(): ProductsDao
    abstract fun subcategoriesDao(): SubCategoriesDao

    companion object {

        private var INSTANCE: EaptekaDb? = null

        fun getInstance(context: Context): EaptekaDb? {
            if (INSTANCE == null) {
                synchronized(EaptekaDb::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        EaptekaDb::class.java,
                        "eapteka.db"
                    )
                        .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}